import  express from "express";
import dotenv from 'dotenv';
import cors from 'cors';
import bodyParser from 'body-parser';
import { Buffer } from 'node:buffer';
import iconvlite from 'iconv-lite';

const app = express();
dotenv.config()
app.use(bodyParser.json());
// app.use(express.bodyParser());
app.use(express.Router());
app.use(cors());
app.use(bodyParser.urlencoded({extends:false}));

const PORT = process.env.PORT || 5552

const getTLV = (tagName,tagValue) => {

    var tagBuf = Buffer.from([tagName]);

    var tagValueLenBuf = Buffer.from([tagValue.length]);

    var tagValueBuf = Buffer.from(tagValue);

    var bufsArray = [tagBuf,tagValueLenBuf,tagValueBuf];
    const totalLength = tagBuf.length + tagValueLenBuf.length + tagValueBuf.length ;
    console.log(`${tagName};${tagValue.length};${tagValue} =====> `,bufsArray)

    return Buffer.concat(bufsArray,totalLength);
}

const generateCodeBase64 = (nsellerName,vatRegistration,timestamp,totalAmount,vatTotalAmount) => {
    var sellerNameBuf = getTLV("01", iconvlite.encode(nsellerName.trim(),'utf-8') );
    var vatRegistrationNameBuf = getTLV("02",vatRegistration.trim());
    var timestampBuf = getTLV("03",new Date(timestamp).toISOString().trim().replace('.000',''));   
    var totalAmountBuf = getTLV("04",parseFloat(totalAmount).toFixed(2));  
    var vatTotalAmountBuf = getTLV("05",parseFloat(vatTotalAmount).toFixed(2));
    
    var tagBufArray = [sellerNameBuf,vatRegistrationNameBuf,timestampBuf,totalAmountBuf,vatTotalAmountBuf];
    const totalLength2 = sellerNameBuf.length + vatRegistrationNameBuf.length + timestampBuf.length + totalAmountBuf.length + vatTotalAmountBuf.length;
    const qrCodeBuf = Buffer.concat(tagBufArray,totalLength2);
    //const qrCodeBufHex = qrCodeBuf.toString('hex')
    console.log(qrCodeBuf.toString('utf-8'),'====>',qrCodeBuf.toString('base64'));
    return qrCodeBuf.toString('base64');
    
}

const getCodeBase64 = (req,res) => {
    const {sellerName,vatRegistration,timestamp,totalAmount,vatTotalAmount} = req.body;
    if(!sellerName || !vatRegistration || !totalAmount || !vatTotalAmount) return res.status(400).json({message: 'Invalid request'});
    //const newSellerName = (sellerName.trim()+'\u200E');
    const code = generateCodeBase64(sellerName,vatRegistration,timestamp,totalAmount,vatTotalAmount)
    res.status(200).json({
        code: code,
    })
}

app.post('/qr-invoice-link-api/generate',getCodeBase64)

// var sellerNameBuf = getTLV("1","test Basement");

// var vatRegistrationNameBuf = getTLV("2","100025906700003");

// var timestampBuf = getTLV("3","15:30:00T2022-04-25");

// var totalAmountBuf = getTLV("4","2100100.99");

// var vatTotalAmountBuf = getTLV("5","315015.15");

// var tagBufArray = [sellerNameBuf,vatRegistrationNameBuf,timestampBuf,totalAmountBuf,vatTotalAmountBuf];

// const qrCodeBuf = Buffer.concat(tagBufArray);

// const qrCodeBase46 = qrCodeBuf.toString('base64');

app.listen(PORT, () => console.log(`listening on port ${PORT} `))


